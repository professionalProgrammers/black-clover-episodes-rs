pub mod download;
pub mod list;

use clap::{
    App,
    ArgMatches,
};

pub fn cli() -> App<'static, 'static> {
    App::new("black-clover-episodes")
        .subcommand(crate::cli::list::cli())
        .subcommand(crate::cli::download::cli())
}

pub fn exec(matches: &ArgMatches) {
    match matches.subcommand() {
        ("list", Some(matches)) => crate::cli::list::exec(&matches),
        ("download", Some(matches)) => crate::cli::download::exec(&matches),
        _ => {
            eprintln!("Expected Subcommand");
        }
    }
}
