use crate::{
    AnimeType,
    BlackError,
    BlackResult,
    Episode,
};
use vidstreaming::{
    StreamData,
    StreamDataSource,
};
use wordpress::Order;

pub struct Client {
    wordpress_client: wordpress::Client,
    vid_client: vidstreaming::Client,
}

impl Client {
    pub fn new() -> Self {
        Self {
            wordpress_client: wordpress::Client::new(
                "https://blackcloverepisodes.com".parse().unwrap(),
            ),
            vid_client: vidstreaming::Client::new(),
        }
    }

    pub async fn list(&self, t: AnimeType) -> BlackResult<Vec<Episode>> {
        let builder = self
            .wordpress_client
            .get_posts_builder()
            .category(t.get_category_number())
            .per_page(100)
            .order(Order::Ascending);

        let posts_list = builder.clone().send().await?;

        let mut posts: Vec<_> = posts_list
            .list
            .into_iter()
            .map(Episode::from_post)
            .collect::<Result<_, _>>()?;

        let total = posts_list.total;

        while posts.len() < total as usize {
            let posts_list = builder.clone().offset(posts.len() as u64).send().await?;

            posts.reserve(total as usize);
            for ep in posts_list.list.into_iter().map(Episode::from_post) {
                posts.push(ep?);
            }
        }

        posts.truncate(total as usize);

        Ok(posts)
    }

    pub async fn get_episode(&self, n: u64, t: AnimeType) -> BlackResult<Episode> {
        let offset = match n.checked_sub(1) {
            Some(o) => o,
            None => {
                return Err(BlackError::MissingEpisode);
            }
        };

        let posts_list = self
            .wordpress_client
            .get_posts_builder()
            .category(t.get_category_number())
            .offset(offset)
            .order(Order::Ascending)
            .send()
            .await?;

        let post = posts_list
            .list
            .into_iter()
            .map(Episode::from_post)
            .next()
            .ok_or(BlackError::MissingEpisode)??;

        if post.slug != Episode::slug_from_number(n) {
            return Err(BlackError::MissingEpisode);
        }

        Ok(post)
    }

    pub async fn get_stream_data(&self, episode: &Episode) -> BlackResult<StreamData> {
        let url = episode.stream_url.as_ref().ok_or(BlackError::MissingLink)?;
        let json = self.vid_client.get_stream_data(url).await?;
        Ok(json)
    }

    pub async fn download(&self, src: &StreamDataSource) -> BlackResult<reqwest::Response> {
        Ok(self.vid_client.download(src).await?)
    }
}

impl Default for Client {
    fn default() -> Self {
        Client::new()
    }
}
