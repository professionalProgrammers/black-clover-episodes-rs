use crate::{
    AnimeType,
    Client,
};
use clap::{
    App,
    Arg,
    ArgGroup,
    ArgMatches,
    SubCommand,
};
use indicatif::{
    ProgressBar,
    ProgressStyle,
};
use std::io::{
    BufWriter,
    Write,
};

pub fn cli() -> App<'static, 'static> {
    SubCommand::with_name("download")
        .arg(Arg::with_name("episode_number").required(true))
        .arg(Arg::with_name("sub").long("sub").short("s"))
        .arg(Arg::with_name("dub").long("dub").short("d"))
        .group(
            ArgGroup::with_name("type")
                .args(&["sub", "dub"])
                .required(true),
        )
}

pub fn exec(matches: &ArgMatches) {
    let mut rt = match tokio::runtime::Builder::new()
        .threaded_scheduler()
        .enable_time()
        .enable_io()
        .build()
    {
        Ok(rt) => rt,
        Err(e) => {
            eprintln!("Failed to start tokio runtime, got error: {:#?}", e);
            return;
        }
    };
    let client = Client::new();
    let n = match matches.value_of("episode_number").unwrap().parse() {
        Ok(n) => n,
        Err(e) => {
            eprintln!("Invalid episode number, got error: {:#?}", e);
            return;
        }
    };
    let at = if matches.is_present("dub") {
        AnimeType::Dub
    } else {
        AnimeType::Sub
    };

    println!("Fetching episode...");
    let ep = match rt.block_on(client.get_episode(n, at)) {
        Ok(e) => e,
        Err(e) => {
            eprintln!("Failed to get episode, got error: {:#?}", e);
            return;
        }
    };
    println!("Located episode: '{}'", ep.title);

    let stream_data = match rt.block_on(client.get_stream_data(&ep)) {
        Ok(d) => d,
        Err(e) => {
            eprintln!("Failed to locate stream data, got error: {:#?}", e);
            return;
        }
    };

    let stream_src = match stream_data.get_default_src() {
        Some(s) => s,
        None => {
            eprintln!("Failed to extract valid stream source");
            return;
        }
    };

    let filename = format!("{}.mp4", ep.slug);

    println!("Downloading '{}'...", filename);
    rt.block_on(async move {
        let mut dl = match client.download(&stream_src).await {
            Ok(dl) => dl,
            Err(e) => {
                eprintln!("Failed to start download, got error: {:#?}", e);
                return;
            }
        };

        let total = dl
            .headers()
            .get(reqwest::header::CONTENT_LENGTH)
            .and_then(|s| s.to_str().ok()?.parse::<u64>().ok());

        let progress = MaybeProgressBar::new(total);

        let out_file = match std::fs::File::create(&filename) {
            Ok(f) => f,
            Err(e) => {
                eprintln!("Failed to open file '{}', got error: {:#?}", filename, e);
                return;
            }
        };

        let mut out_file = BufWriter::new(out_file);

        while let Some(chunk) = {
            match dl.chunk().await {
                Ok(c) => c,
                Err(e) => {
                    eprintln!("Failed to get next chunk, got error: {:#?}", e);
                    return;
                }
            }
        } {
            match out_file.write_all(&chunk) {
                Ok(_) => {}
                Err(e) => {
                    progress.finish();
                    eprintln!(
                        "Failed to write chunk to file '{}', got error: {:#?}",
                        filename, e
                    );
                }
            };

            progress.inc(chunk.len() as u64);
        }
        progress.finish();
    });

    println!("Done.");
}

struct MaybeProgressBar {
    pb: Option<ProgressBar>,
}

impl MaybeProgressBar {
    pub fn new(total: Option<u64>) -> Self {
        let pb = total.map(setup_progress_bar);

        Self { pb }
    }

    pub fn inc(&self, size: u64) {
        if let Some(pb) = self.pb.as_ref() {
            pb.inc(size);
        }
    }

    pub fn finish(&self) {
        if let Some(pb) = self.pb.as_ref() {
            pb.finish();
        }
    }
}

fn setup_progress_bar(total: u64) -> ProgressBar {
    let progress = ProgressBar::new(total);
    progress.set_style(ProgressStyle::default_bar()
        .template("{spinner:.green} [{elapsed_precise}] [{bar:40.cyan/blue}] {bytes}/{total_bytes} ({eta})")
        .progress_chars("#>-"));
    progress
}
