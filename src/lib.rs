#[cfg(feature = "cli")]
pub mod cli;
pub mod client;
pub mod episode;
pub mod error;

pub use crate::{
    client::Client,
    episode::Episode,
    error::{
        BlackError,
        BlackResult,
    },
};
pub use vidstreaming::{
    StreamData,
    StreamDataSource,
};

#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum AnimeType {
    Sub,
    Dub,
}

impl AnimeType {
    pub fn get_category_number(self) -> u64 {
        match self {
            Self::Dub => 2,
            Self::Sub => 1,
        }
    }
}
