use crate::episode::FromPostError;

pub type BlackResult<T> = Result<T, BlackError>;

#[derive(Debug)]
pub enum BlackError {
    Wordpress(wordpress::Error),
    InvalidEpisode(FromPostError),
    VidStreaming(vidstreaming::VidError),

    MissingLink,
    MissingEpisode,
}

impl From<wordpress::Error> for BlackError {
    fn from(e: wordpress::Error) -> Self {
        Self::Wordpress(e)
    }
}

impl From<FromPostError> for BlackError {
    fn from(e: FromPostError) -> Self {
        Self::InvalidEpisode(e)
    }
}

impl From<vidstreaming::VidError> for BlackError {
    fn from(e: vidstreaming::VidError) -> Self {
        Self::VidStreaming(e)
    }
}
