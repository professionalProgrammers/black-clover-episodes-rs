fn main() {
    let matches = black_clover_episodes::cli::cli().get_matches();
    black_clover_episodes::cli::exec(&matches);
}
