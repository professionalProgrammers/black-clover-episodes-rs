use black_clover_episodes::AnimeType;

#[tokio::test]
async fn it_works() {
    let client = black_clover_episodes::Client::new();
    let list = client.list(AnimeType::Dub).await.unwrap();
    dbg!(list.len());
    dbg!(&list[3]);

    let data = client.get_stream_data(&list[3]).await.unwrap();
    dbg!(&data);
    let src = data.get_default_src().unwrap();
    dbg!(&src);
}
