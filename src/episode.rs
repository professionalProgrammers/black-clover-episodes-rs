use select::{
    document::Document,
    predicate::Name,
};
use url::Url;

#[derive(Debug)]
pub enum FromPostError {
    InvalidUrl(url::ParseError),
}

#[derive(Debug)]
pub struct Episode {
    pub title: String,
    pub slug: String,
    pub stream_url: Option<Url>,
}

impl Episode {
    pub fn from_post(post: wordpress::Post) -> Result<Self, FromPostError> {
        let doc = Document::from(post.content.rendered.as_str());
        let stream_url = doc
            .find(Name("iframe"))
            .next()
            .and_then(|el| el.attr("src"))
            .map(|u| Url::parse(u).map_err(FromPostError::InvalidUrl))
            .transpose()?;

        Ok(Self {
            title: post.title.rendered,
            slug: post.slug,
            stream_url,
        })
    }

    pub fn get_number(&self) -> Option<u64> {
        self.slug
            .trim_start_matches("black-clover-episode-")
            .trim_end_matches("-english-dubbed-watch-online")
            .parse()
            .ok()
    }

    pub fn slug_from_number(n: u64) -> String {
        format!("black-clover-episode-{}-english-dubbed-watch-online", n)
    }
}
