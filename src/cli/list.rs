use crate::{
    AnimeType,
    Client,
};
use clap::{
    App,
    Arg,
    ArgGroup,
    ArgMatches,
    SubCommand,
};
use tokio::runtime::Builder;

pub fn cli() -> App<'static, 'static> {
    SubCommand::with_name("list")
        .arg(Arg::with_name("sub").long("sub").short("s"))
        .arg(Arg::with_name("dub").long("dub").short("d"))
        .group(
            ArgGroup::with_name("type")
                .args(&["sub", "dub"])
                .required(true),
        )
}

pub fn exec(matches: &ArgMatches) {
    let mut rt = match Builder::new()
        .threaded_scheduler()
        .enable_time()
        .enable_io()
        .build()
    {
        Ok(rt) => rt,
        Err(e) => {
            eprintln!("Failed to start tokio runtime, got error: {:#?}", e);
            return;
        }
    };

    let client = Client::new();

    let at = if matches.is_present("dub") {
        AnimeType::Dub
    } else {
        AnimeType::Sub
    };

    let at_str = match at {
        AnimeType::Dub => "dubbed",
        AnimeType::Sub => "subbed",
    };

    println!("Fetching {} list...", at_str);
    let list = match rt.block_on(client.list(at)) {
        Ok(r) => r,
        Err(e) => {
            eprintln!("Failed to get list, got error: {:#?}", e);
            return;
        }
    };

    for (i, ep) in list.iter().enumerate() {
        if let Some(url) = ep.stream_url.as_ref() {
            println!("{}) {} [ {} ]", i + 1, ep.title, url);
        } else {
            println!("{}) {}", i + 1, ep.title);
        }
    }
}
